# mBar

## Demo

- [Demo](https://maheee-webstuff.gitlab.io/mbar/)

## Integration

- modern browsers: https://maheee-webstuff.gitlab.io/mbar/build/app.esm.js
- old browsers: https://maheee-webstuff.gitlab.io/mbar/build/app.js

```
  <script type="module" src="./build/app.esm.js"></script>
  <script nomodule src="./build/app.js"></script>
```

## Bookmarklet

```
var script = document.createElement('script');
script.src = 'https://maheee-webstuff.gitlab.io/mbar/dist/mbar.js';
document.body.appendChild(script);
```

```
javascript:(function()%7Bvar%20script%20%3D%20document.createElement('script')%3Bscript.src%20%3D%20'https%3A%2F%2Fmaheee-webstuff.gitlab.io%2Fmbar%2Fdist%2Fmbar.js'%3Bdocument.body.appendChild(script)%7D)()
```
