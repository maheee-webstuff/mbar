import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

// https://stenciljs.com/docs/config

export const config: Config = {
  namespace: "mbar",
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  outputTargets: [
    {
      type: 'www',
      baseUrl: 'https://maheee-webstuff.gitlab.io/mbar/'
      // uncomment the following line to disable service workers in production
      // serviceWorker: null
    },
    {
      type: 'dist',
      baseUrl: 'https://maheee-webstuff.gitlab.io/mbar/dist/'
    }
  ],
  plugins: [
    sass()
  ]
};
