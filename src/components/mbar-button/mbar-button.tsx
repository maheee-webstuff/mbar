import { Component, State, Listen, h } from '@stencil/core';


@Component({
  tag: 'mbar-button',
  styleUrl: 'mbar-button.scss',
  shadow: true
})
export class MbarButton {

  @State() isOpen = false;

  @Listen('mbar__event', { target: 'window' })
  mbarEventHandler(event: CustomEvent) {
    switch (event.detail.action) {
      case 'open':
        this.isOpen = true;
        break;
      case 'close':
        this.isOpen = false;
        break;
    }
  }

  handleClick() {
    if (this.isOpen) {
      window['mbar__close']();
    } else {
      window['mbar__open']();
    }
  }

  render() {
    return (
      <button class={{'open': this.isOpen}} onClick={ () => this.handleClick()}>
        m
      </button>
    );
  }

}
