import { Component, State, h } from '@stencil/core';
import { WeatherState, Forecast } from '../../model/weather.model';
import { WeatherService } from '../../services/weather.service';

@Component({
  tag: 'mbar-weather',
  styleUrl: 'mbar-weather.scss',
  shadow: true
})
export class MbarWeather {
  @State() weatherState = null;
  @State() error = null;

  private weatherSubscription;

  componentDidLoad() {
    const weatherService = WeatherService.Instance();
    this.weatherSubscription = weatherService.getWeather().subscribe(weather => {
      this.weatherState = weather;
      this.error = null;
    }, error => {
      this.weatherState = null;
      this.error = error;
    });
  }

  componentDidUnload() {
    this.weatherSubscription.unsubscribe();
  }

  render() {
    if (!this.weatherState && !this.error) {
      return <div>loading data ...</div>;
    }
    if (this.error) {
      return <div>failed to load data!</div>;
    }
    return (
      <div>
        <h2>Weather in {this.weatherState.title}</h2>
        {this.weatherState.forecasts.map(forecast =>
          <div>
            <h3>{forecast.date}</h3>
            <p class="bold">{forecast.state}</p>
            <p>Temp: {Math.round(forecast.temperature*10)/10}°C</p>
            <p>Hum: {Math.round(forecast.humidity*10)/10}%</p>
            <p>Wind: {Math.round(forecast.windSpeed*10)/10} mph</p>
          </div>
        )}
      </div>
    );
  }

}
