import { Component, State, Listen, Host, Element, h } from '@stencil/core';


@Component({
  tag: 'mbar-container',
  styleUrl: 'mbar-container.scss',
  shadow: true
})
export class MbarContainer {
  @Element() host: HTMLElement;

  @State() isOpen = false;

  @Listen('mbar__event', { target: 'window' })
  mbarEventHandler(event: CustomEvent) {
    switch (event.detail.action) {
      case 'open':
        this.isOpen = true;
        break;
      case 'close':
        this.isOpen = false;
        break;
    }
  }

  close() {
    window['mbar__close']();
  }

  render() {
    return (
      <Host class={{'open': this.isOpen}}>
        <div class="content">
          <div class="header">mBar v0.0.1</div>
          <div class="body">
            <mbar-weather></mbar-weather>
          </div>
          <div class="footer">
            <button class="logout" onClick={ () => this.close()}>close</button>
          </div>
        </div>
      </Host>
    );
  }

}
