
export class Forecast {
  date: string;
  state: string;
  temperature: number;
  humidity: number;
  windSpeed: number;
}

export class WeatherState {
  title: string;
  forecasts: Forecast[];
}
