import { Observable, from } from 'rxjs';
import { shareReplay, switchMap, map } from 'rxjs/operators';
import { WeatherState, Forecast } from '../model/weather.model';

export class WeatherService {
  private static _instance: WeatherService;

  static Instance(): WeatherService {
      return this._instance || (this._instance = new this());
  }

  private weather$: Observable<WeatherState>;

  getWeather(): Observable<WeatherState> {
    if (!this.weather$) {
      this.weather$ = from(fetch('https://www.metaweather.com/api/location/551801/')).pipe(
        switchMap(response => from(response.json())),
        map(data => this.mapWeatherResponse(data)),
        shareReplay(1)
      );
    }
    return this.weather$;
  }

  private mapWeatherResponse(data) {
    const state = {
      title: data.title,
      forecasts: []
    };
    for (let f of data.consolidated_weather) {
      state.forecasts.push({
        date: f.applicable_date,
        temperature: f.the_temp,
        humidity: f.humidity,
        windSpeed: f.wind_speed,
        state: f.weather_state_name
      });
    }
    return state;
  }
}
